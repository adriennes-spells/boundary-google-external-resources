resource "google_kms_key_ring" "boundary" {
  name     = coalesce(var.key_ring_name, var.cluster_name, "boundary")
  location = coalesce(var.key_ring_location, "global")
}

resource "google_kms_crypto_key" "root" {
  name            = "root"
  key_ring        = google_kms_key_ring.boundary.id
  rotation_period = local.rotation_period
}

resource "google_kms_crypto_key" "worker_auth" {
  name            = "worker-auth"
  key_ring        = google_kms_key_ring.boundary.id
  rotation_period = local.rotation_period
}

resource "google_kms_crypto_key" "recovery" {
  name            = "recovery"
  key_ring        = google_kms_key_ring.boundary.id
  rotation_period = local.rotation_period
}

resource "google_kms_crypto_key" "config" {
  name            = "config"
  key_ring        = google_kms_key_ring.boundary.id
  rotation_period = local.rotation_period
}
