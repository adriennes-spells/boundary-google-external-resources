variable "cluster_name" {
  type    = string
  default = ""
}

variable "key_ring_name" {
  type    = string
  default = ""
}

variable "key_ring_location" {
  type    = string
  default = ""
}

locals {
  rotation_period = format("%ds", var.key_rotation_days * 60 * 60 * 24)
}

variable "key_rotation_days" {
  type    = number
  default = 180
}

variable "project" {
  type = string
}

variable "region" {
  type = string
}
